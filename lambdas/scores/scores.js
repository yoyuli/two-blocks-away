const dynamoDbLib = require('./libs/dynamo-lib');
const iotLib = require('./libs/iot-lib');

const SCORES_TABLE_NAME=process.env.SCORES_TABLE_NAME;
const RANKING_INDEX_NAME=process.env.RANKING_INDEX_NAME;

// this function is triggered whenever a score is changed in dynamodb
module.exports.change = async (event, context, callback) => {
  try {
    const topScores = await getTopScores();
    publishScores(topScores); // publish these scores to a device shadow (also could publish to a topic)
    callback(null, 'This was a success!');
  } catch (e) {
    console.error(e,e.stack);
    callback(true, 'There was an error!');
  }
};

const getTopScores = async () => {
  const params = {
    TableName: SCORES_TABLE_NAME,
    IndexName:RANKING_INDEX_NAME,
    KeyConditionExpression:'game = :TBA AND score > :ZERO',
    ExpressionAttributeValues:{
      ':TBA':'TBA',
      ':ZERO':0
    },
    ScanIndexForward:false, // the traversal is performed in descending order
    Limit:10
  };
  return sortScores((await dynamoDbLib.call('query', params)).Items).slice(0, 10);
}

const publishScores = (scores) => {
  iotLib.publish(process.env.IOT_LEADERBOARD, scores);
}

const sortScores = (items) => {
  return items.sort((a, b) => {
    return (a.score > b.score) ? -1 : ((a.score < b.score) ? 1 : 0);
  });
}
