const AWS = require('aws-sdk');

AWS.config.update({ region: process.env.AWS_REGION });

module.exports.call = (action, params) => {
  const dynamoDb = new AWS.DynamoDB.DocumentClient();

  return dynamoDb[action](params).promise();
}
