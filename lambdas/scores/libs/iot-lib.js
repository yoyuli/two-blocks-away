const AWS = require('aws-sdk');
const iotdata = new AWS.IotData({ endpoint: process.env.IOT_ENDPOINT });

module.exports.publish = (thingName, message) => {
  const update = {
    "state": {
      "desired": {
        "status": message
      }
    }
  };

  iotdata.updateThingShadow({
    payload: JSON.stringify(update),
    thingName: thingName
  }, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      console.log(data);
    }
  });
}
