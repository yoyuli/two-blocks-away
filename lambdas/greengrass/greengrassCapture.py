# This is the Greengrass Lambda function used to capture images using RPI camera
# Copyright 2018 Cloudreach or its affiliates. All Rights Reserved.
#

# greengrassCapture.py
# import greengrasssdk
import logging
import time
import picamera
import boto3
import json
import os

IMAGE_BUCKET_NAME=os.environ['IMAGE_BUCKET_NAME']

REGION=os.environ['AWS_DEFAULT_REGION']

# Creating a greengrass core sdk client
# client = greengrasssdk.client('iot-data')
iotData = boto3.client('iot-data',
            region_name=REGION)

# Configure logging
logger = logging.getLogger("greengrassCapture")

CALLBACK_TOPIC=os.environ['CALLBACK_TOPIC']
THING_NAME=os.environ['THING_NAME']

def send_message(topic,message):
    iotData.publish(topic= topic, payload=json.dumps(message))

def getLastCapturedTime():
    response = iotData.get_thing_shadow(
        thingName=THING_NAME
    )
    shadow=json.loads(response['payload'].read())

    lastCapturedTime=0
    if shadow['state']['desired'].has_key('lastCapturedTime'):
        lastCapturedTime=shadow['state']['desired']['lastCapturedTime']
        logger.info('lastCapturedTime '+ str(lastCapturedTime))
    else:
        logger.info('lastCapturedTime not defined, set to 0')
    
    return lastCapturedTime

def updateLastCapturedTime(timestamp):
    logger.info('Updating lastCapturedTime...')
    response = iotData.update_thing_shadow(
        thingName=THING_NAME,
        payload=json.dumps({
            "state":{
                "desired":{
                    "lastCapturedTime":timestamp
                }
            }
        })
    )
    logger.info(response)
    return


def capture():
    logger.info('Trying to capture')
    with picamera.PiCamera() as camera:
        start = time.time()
        camera.resolution = (1024, 768)
        camera.framerate = 30
        # camera.start_preview()
        camera.capture_sequence([
            ('/local/test-rapid-image-capture.jpg'),
            ], use_video_port=True)
        finish = time.time()
        logger.info(finish - start)
    return

def upload(buttonPushedTime):
    s3 = boto3.client('s3')
    logger.info('Uploading image...')
    with open('/local/test-rapid-image-capture.jpg', 'rb') as data:
        s3.upload_fileobj(data, IMAGE_BUCKET_NAME, str(int(buttonPushedTime))+'.jpg')
        logger.info('Image uploaded')
    return 


# This is the Lambda handler 
def function_handler(event, context):
    logger.debug('Lambda function is triggered')
    logger.debug(event)
    lastCapturedTime=getLastCapturedTime()
    if(event.has_key('buttonPushed') and event['buttonPushed']>lastCapturedTime):
        buttonPushedTime=event['buttonPushed']
        updateLastCapturedTime(buttonPushedTime)
        capture()
        upload(buttonPushedTime)
        send_message(CALLBACK_TOPIC,event)
    else:
        logger.error('Event already processed.')
    return