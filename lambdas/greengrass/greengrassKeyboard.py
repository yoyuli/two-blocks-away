'''
/*
 * This is a WIP attempting to use Greengrass to deploy the script,
 * which captures the user input from a generic USB keyboard
 */
 '''

import greengrasssdk
import platform
import logging

import string
import os
import sys
import time

from evdev import InputDevice
from select import select

# Creating a greengrass core sdk client
client = greengrasssdk.client('iot-data')

# Retrieving platform information to send from Greengrass Core
my_platform = platform.platform()

# Configure logging
logger = logging.getLogger("pi/keyboard")
logger.setLevel(logging.DEBUG)

ENTER_KEY=28
THING_NAME=os.environ['THING_NAME']
CALLBACK_TOPIC=os.environ['CALLBACK_TOPIC']

def getGameStatus():
    gameStatus=None
    logger.info('Getting things shadow')
    response = client.get_thing_shadow(
        thingName=THING_NAME
    )
    logger.info('Got things shadow')
    shadow=json.loads(response['payload'].read())

    if shadow['state']['desired'].has_key('status'):
        gameStatus=shadow['state']['desired']['status']
        logger.info('Game status: '+ gameStatus)
    else:
        logger.info('Game status not defined')
    return gameStatus

def pushMessage():
        gameStatus=getGameStatus()
        if (gameStatus is None) or gameStatus == 'waiting':
                if(gameStatus is None):  # Handling situations when game status is not yet set           
                        logger.warning("Game status is not set")
                logger.info("Publishing to topic Hackathon/Game/ButtonPush")
                response = client.publish(
                        CALLBACK_TOPIC=CALLBACK_TOPIC,
                        payload=json.dumps({
                        'buttonPushed':time.time()
                        })
                )
        else:
                logger.info("Invalid game status, doing nothing")
                
def run():
    # look for a /dev/input/by-id/usb...kbd or something similar
    DEVICE = "/dev/input/by-id/usb-SIGMACHIP_USB_Keyboard-event-kbd"

    dev = InputDevice(DEVICE)

    while True:

        # wait for keypad command
        r, w, x = select([dev], [], [])

        # read keypad
        for event in dev.read():
            if event.type==1 and event.value==1:
                    logger.debug("KEY CODE: " + str(event.code))
                    if(event.code==ENTER_KEY):
                        logger.info("ENTER key pressed")
                        pushMessage()

run()

# This is a dummy handler and will not be invoked
# Instead the code above will be executed in an infinite loop for our example
def function_handler(event, context):
    return 

