'''
/*
 * This is a WIP attempting to use Greengrass to deploy the script,
 * which captures the user input from Dream Cheeky 902 Big Red Button
 * However, there was an issue related to accessing this USB device in Greengrass functions
 */
 '''

import greengrasssdk
import platform
import logging
from cheeky import Button

# Creating a greengrass core sdk client
client = greengrasssdk.client('iot-data')

# Retrieving platform information to send from Greengrass Core
my_platform = platform.platform()

# Configure logging
logger = logging.getLogger("pi/button")


class MyButton(Button):
    def button_down(self):
        logger.info("The button is down")
        send_message("The button is down")

    def button_up(self):
        print "The button is up"

    def lid_down(self):
        print "The lid is down"

    def lid_up(self):
        print "The lid is up"


def send_message(message):
    if not my_platform:
        client.publish(topic='pi/camera', payload=message+' - from Button .')
    else:
        client.publish(topic='pi/camera', payload=message+' - from Button on platform: {}'.format(my_platform))



def run():
    btn = MyButton()
    btn.connect()

    while 1:
        btn.heartbeat()

    btn.disconnect()

run()

# This is a dummy handler and will not be invoked
# Instead the code above will be executed in an infinite loop for our example
def function_handler(event, context):
    return 