const AWS = require('aws-sdk');

const rekognition = new AWS.Rekognition({
  region:process.env.AWS_REGION,
  apiVersion: '2016-06-27'
});

const iotdata = new AWS.IotData({ 
  endpoint: process.env.IOT_ENDPOINT,
  region:process.env.AWS_REGION,
  apiVersion: '2015-05-28'
});

const dynamodb=new AWS.DynamoDB({
  region:process.env.AWS_REGION
});

const BUCKET_NAME=process.env.BUCKET_NAME;
const GAME_LENGTH=process.env.GAME_LENGTH?process.env.GAME_LENGTH:90;
const SOLUTION_TOPIC=process.env.SOLUTION_TOPIC;
const SCORE_TABLE_NAME=process.env.SCORE_TABLE_NAME;


const match=require('./matching');

class Steps{
  constructor(){
    this.thingName= process.env.THING_NAME;
  }
  
  validateTimestamp(event){
    console.log('Start processing event: '+ JSON.stringify(event));
    let _this=this;
    this.buttonPushedTime=event.buttonPushed;
    return new Promise((resolve,reject)=>{
      let params = {
        thingName: _this.thingName
      };
      iotdata.getThingShadow(params).promise()
      .then((shadow)=>{
        let payload=JSON.parse(shadow.payload); 
        let lastProcessedTime=0;
        if(payload.state && payload.state.desired && 'lastProcessedTime' in payload.state.desired){
          lastProcessedTime=payload.state.desired.lastProcessedTime;
        } // Getting lastProcessedTime in game device shadow
        console.log('lastProcessedTime: '+ lastProcessedTime);
        if(_this.buttonPushedTime && _this.buttonPushedTime>lastProcessedTime){
          resolve(event);
        } // If button is pushed after lastProcessedTimestamp, resolve
        else if(event.backdoorMode){
          resolve(event);
        }
        else{
          reject('Timestamp already processed');
        } //  Otherwise, reject
      });
    });
  }

  updateTimestamp(){
    let payload={
      state:{
        desired:{
          lastProcessedTime:this.buttonPushedTime
        }
      }
    };
    let params = {
      thingName: this.thingName,
      payload:JSON.stringify(payload)
    };
    return iotdata.updateThingShadow(params).promise();
  }
  
  getGameState(){
    let _this=this;
    return new Promise((resolve,reject)=>{
      var params = {
        thingName: _this.thingName
      };
      iotdata.getThingShadow(params).promise()
      .then((shadow)=>{
        let payload=JSON.parse(shadow.payload); 
        if(payload.state && payload.state.desired && 'level' in payload.state.desired){
          _this.gameState=payload.state.desired;
        }
        else{
          reject('Level must be set');
        }
        resolve();
      })
      .catch(reject);
    });
  }
  
  detectText(){
    var params = {
      Image: { 
        S3Object: {
          Bucket: BUCKET_NAME,
          Name: Math.floor(this.buttonPushedTime)+'.jpg'
        }
      }
    };
    console.log('Calling Rekognition: '+ JSON.stringify(params));
    return rekognition.detectText(params).promise();
  }
  
  matchSolution(rekognitionResponse){
    let level=this.gameState.level;
    return new Promise((resolve)=>{
      let result= match(rekognitionResponse,level);
      resolve(result);
    });
  }

  calculateScore(result){
    this.result=result;
    if(result && this.gameState){
      let state=this.gameState;
      let levelsCompleted=state.levelMarker?
      Object.keys(state.levelMarker)
      .map((key)=> state.levelMarker[key].length)
      .reduce((acc,length)=>(acc+length),0)+1
        :0;
      let time=state.time?state.time:0;
      this.gameState.levelsCompleted=levelsCompleted;
      this.gameState.score=Number(levelsCompleted)*GAME_LENGTH+Number(time);

    }
    return new Promise((resolve)=>{
      resolve();
    });
  }
  
  publishResult(){
    let params = {
      topic: SOLUTION_TOPIC,
      payload: JSON.stringify({
        isCorrect: this.result,
        levelsCompleted: this.gameState.levelsCompleted,
        score: this.gameState.score,
        buttonPushed:this.buttonPushedTime
      }),
      qos: 0
    };
    console.log('result: '+this.result)
    return iotdata.publish(params).promise();
  }

  putItemInDynamo(){
    let _this=this;
    if(this.result && this.gameState.playerId){
      var params = {
        Item: {
          "game":{
            S:"TBA"
          },
          "score": {
            N:_this.gameState.score? _this.gameState.score.toString():'0'
            },
          "id":{
            S: _this.gameState.playerId
            },
          "playerName":{
            S:_this.gameState.name?_this.gameState.name:'Stranger'
          },
          "time":{
            N:_this.gameState.time?_this.gameState.time.toString():'0'
          },
          "level":{
            N:_this.gameState.levelsCompleted?_this.gameState.levelsCompleted.toString():'0'
          }
        }, 
        TableName: SCORE_TABLE_NAME
       };
       return dynamodb.putItem(params).promise();
    }else {
      return new Promise((resovle,reject)=>{
        reject('Skipping putting in DynamoDB')
      });
    }
  }
  
}

module.exports=Steps;