const answers=require('./answers.js');


var boundaryBox={
    a:{X:process.env.a_x?process.env.a_x:0,Y:process.env.a_y?process.env.a_y:0},    // top left
    b:{X:process.env.b_x?process.env.b_x:1,Y:process.env.b_y?process.env.b_y:0},    // top right
    c:{X:process.env.c_x?process.env.c_x:1,Y:process.env.c_y?process.env.c_y:1},    // bottom right
    d:{X:process.env.d_x?process.env.d_x:0,Y:process.env.d_y?process.env.d_y:1}     // bottom left
}

const MATCH_RADIUS=process.env.MATCH_RADIUS?process.env.MATCH_RADIUS:0.3;

/*
// sampleResponse:
// TextDetections: Array
// individual objects
// - Type: we need "WORD"
// - DetectedText 
// - Geometry
//      - BoundingBox
//      - Polygon: Point array
//          -   Point objects {X:...,Y:...}
*/

const extractWords=(TextDetections)=>{
    var words={};
    TextDetections.map((textObject)=>{
        if(textObject.Type=='WORD'){
            let originalCoordinates=getPolygonCentre(textObject.Geometry.Polygon);
            let newCoordinates=justifiedCoordinates(originalCoordinates,boundaryBox);
            words[textObject.DetectedText]=newCoordinates;
        }
    });
    return words;
    /*
    words={
        'WORD1':{X:...,Y:...},
        'WORD2':{X:...,Y:...}
    }
    */
}

const getPolygonCentre=(polygon)=>{
    var x=((polygon[0].X+polygon[3].X)/2+(polygon[1].X+polygon[2].X)/2)/2;
    var y=((polygon[0].Y+polygon[1].Y)/2+(polygon[2].Y+polygon[3].Y)/2)/2;
    return {
        X:x,
        Y:y
    }
}

const getDistance=(dot1,dot2)=>{
    var d1=Math.abs(dot1.X-dot2.X);
    var d2=Math.abs(dot1.Y-dot2.Y);
    var s1=Math.pow(d1,2);
    var s2=Math.pow(d2,2);
    return Math.sqrt(s1+s2)
}

const getBoundaryBoxSize=(boundaryBox)=>{
    let H1=getDistance(boundaryBox.a,boundaryBox.d);
    let H2=getDistance(boundaryBox.b,boundaryBox.c);
    let W1=getDistance(boundaryBox.a,boundaryBox.b);
    let W2=getDistance(boundaryBox.c,boundaryBox.d);
    return {
        W:(W1+W2)/2,
        H:(H1+H2)/2
    }
}

const justifiedCoordinates=(dot,boundaryBox)=>{
    let size=getBoundaryBoxSize(boundaryBox);
    return {
        X:Math.abs(dot.X-boundaryBox.a.X)/size.W,
        Y:Math.abs(dot.Y-boundaryBox.a.Y)/size.H
    }
};

const matchAnswer=(answers,words)=>{
    let matched=[];
    for(answer of answers){
        for(word in words){
            if(word.includes(answer.name)){
                console.log('Found '+answer.name);
                var distance=getDistance(words[word],answer.position);
                console.log('Distance: '+distance);
                if(distance<MATCH_RADIUS) {
                    console.log('Matched '+answer.name);
                    matched.push(answer);
                    break;
                }else{
                    console.log('Incorrect position: '+answer.name)
                }   
            }
        }
    }
    console.log('Matched words: '+JSON.stringify(matched));
    let matchingOutcome=(matched.length==answers.length);
    console.log('Matching outcome: '+matchingOutcome);
    return matchingOutcome;
}

const validateResult=(detectionResult,level)=>{
    if (detectionResult.TextDetections && detectionResult.TextDetections.length>0){
        if(answers[level]){
            var words= extractWords(detectionResult.TextDetections);
            return matchAnswer(answers[level.toString()],words);
        }else{
            console.error('Level name not found');
            return false;
        }
    }else{
        console.error('Text not detected');
        return false
    }
}

module.exports=validateResult;