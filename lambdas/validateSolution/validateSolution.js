'use strict';

const Steps=require('./validationSteps');

const run=(event, context,callback)=>{
  const steps=new Steps();
  steps.validateTimestamp(event)
  .then(steps.updateTimestamp.bind(steps))
  .then(steps.getGameState.bind(steps))
  .then(steps.detectText.bind(steps))
  .then(steps.matchSolution.bind(steps))
  .then(steps.calculateScore.bind(steps))
  .then(steps.publishResult.bind(steps))
  .then(steps.putItemInDynamo.bind(steps))
  .then(()=>{
    callback(null,'Validation steps complete');
  })
  .catch(callback);
}

module.exports.run=run;