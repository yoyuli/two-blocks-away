var Answers={
    "twoTierWeb":[
        {
            name:'Rel',
            position:{X:0.24,Y:0.47}
        }
    ],
    "serverlessWeb":[
         {
            name:'API',
            position:{X:0.49,Y:0.48}
        },
        {
            name:'ron',
            position:{X:0.65,Y:0.49}
        }
    ],
    "serverlessMobile":[
        {
            name:'Dy',
            position:{X:0.18,Y:0.48}
        },
        {
            name:'og',
            position:{X:0.65,Y:0.75}
        }
    ],
    "eventStream":[
        {
            name:'tre',
            position:{X:0.33,Y:0.45}
        },
        {
            name:'Gla',
            position:{X:0.8,Y:0.73}
        }
       
    ],
    "centralisedLog":[
        {
            name:'atch',
            position:{X:0.33,Y:0.31}
        },
        {
            name:'3',
            position:{X:0.32,Y:0.88}
        }
        
    ],
    "twoBlocks":[
        {
            name:'Gre',
            position:{X:0.53,Y:0.32}
        },
        {
            name:'Lam',
            position:{X:0.37,Y:0.6}
        }
    ],
    "machineLearning":[
        {
            name:'age',
            position:{X:0.56,Y:0.46}
        },
        {
            name:'3',
            position:{X:0.22,Y:0.74}
        }
    ],
    "dataLake":[
        {
            name:'Lam',
            position:{X:0.33,Y:0.74}
        },
        {
            name:'Ath',
            position:{X:0.64,Y:0.46}
        }
    ],
    "continuousDeployment":[
        {
            name:'For',
            position:{X:0.56,Y:0.74}
        },
        {
            name:'tai',
            position:{X:0.72,Y:0.46}
        }
    ],
    "vdi":[
        {
            name:'Spa',
            position:{X:0.71,Y:0.46}
        },
        {
            name:'atch',
            position:{X:0.72,Y:0.74}
        }
    ],
    "cloudBackup":[
        {
            name:'Sto',
            position:{X:0.4,Y:0.6}
        },
        {
            name:'ect',
            position:{X:0.56,Y:0.59}
        }
    ],
    "containerBased":[
        {
            name:'Bal',
            position:{X:0.6,Y:0.6}
        },
        {
            name:'API',
            position:{X:0.77,Y:0.59}
        }
    ],
    "inMemoryCache":[
        {
            name:'EMR',
            position:{X:0.4,Y:0.6}
        },
        {
            name:'ache',
            position:{X:0.56,Y:0.59}
        }
    ],
    "vod":[
        {
            name:'Dy',
            position:{X:0.33,Y:0.75}
        },
        {
            name:'Ele',
            position:{X:0.48,Y:0.48}
        }
    ],
    "iotPayload":[
        {
            name:'Gre',
            position:{X:0.17,Y:0.5}
        },
        {
            name:'Que',
            position:{X:0.48,Y:0.51}
        }
    ],
    "continuousSecurity":[
        {
            name:'fig',
            position:{X:0.17,Y:0.75}
        },
        {
            name:'Not',
            position:{X:0.48,Y:0.6}
        },
        {
            name:'Lam',
            position:{X:0.64,Y:0.88}
        }
    ],
    "oracleMigration":[
        {
            name:'Sno',
            position:{X:0.33,Y:0.46}
        },
        {
            name:'Mig',
            position:{X:0.64,Y:0.74}
        }
    ],
    "cloudApplication":[
        {
            name:'ect',
            position:{X:0.32,Y:0.6}
        },
        {
            name:'File',
            position:{X:0.64,Y:0.6}
        },
        {
            name:'Rel',
            position:{X:0.79,Y:0.88}
        }
    ],
    "recommendationEngine":[
        {
            name:'AppSync',
            position:{X:0.4,Y:0.51}
        },
        {
            name:'Neptune',
            position:{X:0.72,Y:0.65}
        }
    ]


}

module.exports=Answers;