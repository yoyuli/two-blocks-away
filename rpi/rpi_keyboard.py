'''
/*
 * This is the script used to capture user input using the ENTER key of a generic USB keyboard
 * Make sure the device name matches the keyboard you use
 */
 '''

import boto3
import logging
import json
import string
import os
import sys
import time

from evdev import InputDevice
from select import select

# The name of the USB device
# To be replaced with the name of the specific device you use
# You can use `ls /dev/input/by-id/` to find out
# look for a /dev/input/by-id/usb...kbd or something similar
DEVICE = "/dev/input/by-id/usb-SIGMACHIP_USB_Keyboard-event-kbd"
REGION='eu-west-1'
THING_NAME='TBA_Game_test'
TOPIC_NAME='TBA/test/ButtonPush'

# Creating a greengrass core sdk client
client = boto3.client('iot-data',REGION)

# Configure logging
logging.basicConfig(filename='/tmp/rpi_keyboard.log',level=logging.DEBUG)
logger = logging.getLogger("pi/keyboard")

ENTER_KEY=28

def getGameStatus():
    gameStatus=None
    logger.debug('Getting things shadow %s in region %s' % (THING_NAME,REGION))
    response = client.get_thing_shadow(
        thingName=THING_NAME
    )
    logger.debug('Got things shadow')
    shadow=json.loads(response['payload'].read())

    if shadow['state']['desired'].has_key('gameStatus'):
        gameStatus=shadow['state']['desired']['gameStatus']
        logger.info('Game status: '+ gameStatus)
    else:
        logger.info('Game status not defined')
    return gameStatus

def pushMessage():
        gameStatus=getGameStatus()
        if (gameStatus is None) or gameStatus == 'waiting':
                if(gameStatus is None):  # Handling situations when game status is not yet set           
                        logger.warning("Game status is not set")
                logger.info("Publishing to topic"+TOPIC_NAME)
                response = client.publish(
                        topic=TOPIC_NAME,
                        payload=json.dumps({
                        'buttonPushed':time.time()
                        })
                )
        else:
                logger.info("Invalid game status, doing nothing")
                
def run():
    dev = InputDevice(DEVICE)

    while True:

        # wait for keypad command
        r, w, x = select([dev], [], [])

        # read keypad
        for event in dev.read():
            if event.type==1 and event.value==1:
                    logger.debug("KEY CODE: " + str(event.code))
                    if(event.code==ENTER_KEY):
                        logger.info("ENTER key pressed")
                        pushMessage()

run()
