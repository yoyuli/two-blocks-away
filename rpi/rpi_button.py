'''
/*
 * This is the script used to capture user input using Dream Cheeky 902 Big Red Button
 * This hardware product is discountinued
 * To be used with generic USB keyboard, see rpi_keyboard.py
 */
 '''

import boto3
import logging
import time
import json
from cheeky import Button

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

client = boto3.client('iot-data')



def getGameStatus():
    gameStatus=None
    response = client.get_thing_shadow(
        thingName='TBA_Core'
    )
    shadow=json.loads(response['payload'].read())

    if shadow['state']['desired'].has_key('status'):
        gameStatus=shadow['state']['desired']['status']
        logger.info('Game status: '+ gameStatus)
    else:
        logger.info('Game status not defined')
    return gameStatus

class MyButton(Button):
    def button_down(self):
        logger.info("The button is down")
        gameStatus=getGameStatus()
        if (gameStatus is None) or gameStatus == 'waiting':
            if(gameStatus is None):  # Handling situations when game status is not yet set           
                logger.warning("Game status is not set")
            logger.info("Publishing to topic Hackathon/Game/ButtonPush")
            response = client.publish(
                topic='Hackathon/Game/ButtonPush',
                payload=json.dumps({
                    'buttonPushed':time.time()
                })
            )
        else:
            logger.info("Invalid game status, doing nothing")

    def button_up(self):
        print "The button is up"

    def lid_down(self):
        print "The lid is down"

    def lid_up(self):
        print "The lid is up"

def run():
    btn = MyButton()
    btn.connect()


    while 1:
        btn.heartbeat()

    btn.disconnect()

run()