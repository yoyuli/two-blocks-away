#! /bin/sh
# /etc/init.d/TBA

### BEGIN INIT INFO
# Provides:          TBA
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Simple script to start a program at boot
### END INIT INFO

# Usage
# sudo chmod 755 /etc/init.d/NameOfYourScript
# sudo update-rc.d NameOfYourScript defaults

# If you want a command to always run, put it here

# Carry out specific functions when asked to by the system
case "$1" in
  start)
    echo "Starting greengrass"
    /greengrass/ggc/core/greengrassd start
    echo "Starting rpi_keyboard.py. Logs can be found in /tmp."
    python /home/pi/rpi_button/rpi_keyboard.py &
    # AWS credentials to be configured in /etc/boto.cfg
    ;;
  stop)
    echo "Stopping greengrass"
    # kill application you want to stop
    /greengrass/ggc/core/greengrassd stop
    ;;
  *)
    echo "Usage: /etc/init.d/TBA {start|stop}"
    exit 1
    ;;
esac

exit 0