const iot = require('aws-iot-device-sdk');
const AWS = require('aws-sdk');
//
// Remember our current subscription topic here.
//
const clientId = `mqtt-hackathon-device-${  Math.floor((Math.random() * 100000) + 1)}`;

AWS.config.region = process.env.REACT_APP_AWS_REGION;

AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: process.env.REACT_APP_AWS_IDENTITY_POOL_ID
});

//
// Create the AWS IoT device object.  Note that the credentials must be
// initialized with empty strings; when we successfully authenticate to
// the Cognito Identity Pool, the credentials will be dynamically updated.
//
const device = iot.device({
  //
  // Set the AWS region we will operate in.
  //
  region: AWS.config.region,
  //
  ////Set the AWS IoT Host Endpoint
  host: process.env.REACT_APP_AWS_IOT_ENDPOINT,
  //
  // Use the clientId created earlier.
  //
  clientId: clientId,
  //
  // Connect via secure WebSocket
  //
  protocol: 'wss',
  //
  // Set the maximum reconnect time to 8 seconds; this is a browser application
  // so we don't want to leave the user waiting too long for reconnection after
  // re-connecting to the network/re-opening their laptop/etc...
  //
  maximumReconnectTimeMs: 8000,
  //
  // Enable console debugging information (optional)
  //
  debug: true,
  //
  // IMPORTANT: the AWS access key ID, secret key, and sesion token must be
  // initialized with empty strings.
  //
  accessKeyId: '',
  secretKey: '',
  sessionToken: ''
});

device.on('close', () => {
  console.log('Closing connection to Amazon IoT');
});

device.on('connect', () => {
  console.log('Connecting to Amazon IoT');
  device.subscribe(process.env.REACT_APP_AWS_IOT_TOPIC_GAME_START);
  device.subscribe(process.env.REACT_APP_AWS_IOT_TOPIC_GAME_FINISH);
  device.subscribe(process.env.REACT_APP_AWS_IOT_TOPIC_GAME_SOLUTION);
  device.subscribe(process.env.REACT_APP_AWS_IOT_TOPIC_GAME_CHECK);
  device.subscribe(process.env.REACT_APP_AWS_IOT_TOPIC_LEADERBOARD_CHANGE);
});

device.on('reconnect', () => {
  console.log('Attempting to reconnect to Amazon IoT');
});

device.on('error', err => {
  console.log(err);
});

device.on('offline', () => {
  console.log('Device is offline');
});

const cognitoIdentity = new AWS.CognitoIdentity();
AWS.config.credentials.get(function (err, data) {
  if (!err) {
    console.log(`retrieved identity: ${  AWS.config.credentials.identityId}`);
    const params = {
      IdentityId: AWS.config.credentials.identityId
    };
    cognitoIdentity.getCredentialsForIdentity(params, function (err, data) {
      if (!err) {
        //
        // Update our latest AWS credentials; the MQTT client will use these
        // during its next reconnect attempt.
        //
        device.updateWebSocketCredentials(data.Credentials.AccessKeyId,
          data.Credentials.SecretKey,
          data.Credentials.SessionToken);
      } else {
        console.log(`error retrieving credentials: ${  err}`);
        alert(`error retrieving credentials: ${  err}`);
      }
    });
  } else {
    console.log(`error retrieving identity:${  err}`);
    alert(`error retrieving identity: ${  err}`);
  }
});

export default device;
