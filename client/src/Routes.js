import React, {Component} from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import Game from '../src/components/Game/Game';
import Leaderboard from '../src/components/Leaderboard/Leaderboard';
import NotFound from '../src/components/NotFound/NotFound';
import Signup from '../src/components/Signup/Signup';

class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route
          path="/signup"
          exact
          component={Signup} />
        <Route
          path="/leaderboard"
          exact
          component={Leaderboard} />
        <Route
          path="/game"
          exact
          component={Game} />
        />
        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default withRouter(Routes);
