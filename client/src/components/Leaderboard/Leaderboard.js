import React, { Component } from 'react';
import './Leaderboard.css';
import shadows from '../../libs/iot-shadow';
import AWS from 'aws-sdk';
import logo from './images/leaderboard-logo.svg';

export default class Leaderboard extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      scores: []
    }
  }

  async initTableData() {
    try {
      // invoke lambda function that calculates leaderboard
      await ((new AWS.Lambda()).invoke({ FunctionName: process.env.REACT_APP_AWS_LAMBDA_LEADERBOARD }).promise());
    } catch (e) {
      console.error('Error initializing table data');
    }
  }

  componentDidMount() {
    shadows.on('connect', () => {
      shadows.register(process.env.REACT_APP_AWS_IOT_SHADOW_LEADERBOARD, () => {
        this.initTableData();
      });
    });

    shadows.on('delta', (name, stateObject) => {
      if (name === process.env.REACT_APP_AWS_IOT_SHADOW_LEADERBOARD) {
        this.setState({ scores: stateObject.state.status });
      }
    });
  }

  render() {
     var seq=1;
    return (
      <div className="Leaderboard">
      <div id="left-panel">
        <img src={logo} alt="tba logo"></img>
      </div>
      <article>
      <table className="Leaderboard__Table">
          <tbody>
            <tr>
              <th></th>
              <th>Name</th>
              <th>Levels Completed</th>
              <th>Score</th>
            </tr>
            {
              this.state.scores.map( ({ id,playerName, level,score }) => {
                return (
                  <tr key={id}>
                    <td class="number">{seq++}</td>
                    <td class="player-name">{playerName}</td>
                    <td class="level">{level}</td>
                    <td class="score">{score}</td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
      </article>
        
      </div>
    );
  }
}
