import React, { Component } from 'react';
import './Signup.css';
import * as dynamo from '../../libs/dynamo';
import device from '../../libs/iot-device';

import uuidv1 from 'uuid/v1';

export default class Signup extends Component {
  state = {
    firstName: '',
    lastName: '',
    email: '',
    signupEnabled: true
  };
  
  validateForm() {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return (
      this.state.firstName.length > 0 &&
      this.state.lastName.length > 0 &&
      this.state.email.length > 0 && 
      re.test(String(this.state.email).toLowerCase())
      );
    }
    
    handleChange(e) {
      this.setState({
        [e.target.name]: e.target.value
      });
    }
    
    async handleSubmit(e) {
      
      if (!this.state.signupEnabled) {
        alert('Game currently in progress!')
        return;
      }
      
      if(!this.validateForm()){
        console.log('Invalid form submission');
        this.setState({ invalidForm: true});
        return;
      }else{
        this.setState({ invalidForm: false});
      }
      
      try {
        const playerId = uuidv1();
        const params = {
          TableName: process.env.REACT_APP_AWS_DYNAMODB_USERS,
          Item: {
            id: playerId,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email
          }
        };
        await dynamo.call('put', params);
        device.publish(process.env.REACT_APP_AWS_IOT_TOPIC_GAME_START, JSON.stringify({
          playerName: this.state.firstName,
          playerId: playerId
        }));
        this.setState({
          firstName: '',
          lastName: '',
          email: '',
          signupEnabled: false
        });
      } catch (e) {
        console.error(e);
      }
    }
    
    componentDidMount() {
      device.on('message', (topic, payload) => {
        if (topic === process.env.REACT_APP_AWS_IOT_TOPIC_GAME_FINISH) {
          this.setState({ signupEnabled: true});
        }
      });
    }
    
    render() {
      return (
        
        
        this.state.signupEnabled ? 
        (
          <div className="Signup__Container">
          <form className="Signup__Form">
          <h1 className="signup-text">sign up</h1>
          <p className="signup-disclosure">
          Please input your details to participate in this game. Don’t worry,
          we will only be using your information to contact you about the
          game.
          </p>
          <div className="Signup_Input">
          <div className="Signup__FirstName">
          <input
          id="first_name"
          className="Signup__FirstName__Input"
          name="firstName"
          placeholder="first name"
          value={this.state.firstName}
          onChange={this.handleChange.bind(this)}
          />
          </div>
          <div className="Signup__LastName">
          <input
          id="last_name"
          className="Signup__LastName__Input"
          name="lastName"
          placeholder="last name"
          value={this.state.lastName}
          onChange={this.handleChange.bind(this)}
          />
          </div>
          <div className="Signup__Email">
          <input
          id="email"
          className="Signup__Email__Input"
          name="email"
          placeholder="email"
          value={this.state.email}
          onChange={this.handleChange.bind(this)}
          />
          </div>
          {
            this.state.invalidForm?
            (<div class="warning-message">Oops, please check your input</div>):''
          }
          </div>
          <div className="Signup__LoaderBtn__Container">
          <a text="Signup" className="Signup__LoaderBtn" onClick={this.handleSubmit.bind(this)}>let's play</a>
          </div>
          
          </form>
          </div>
          ):(
            <div className="Instruction-Container">
            <div className="Instruction">
            <h3>How to play</h3>
            <ul>
            <li>The game will start in <b>10 seconds</b> on the main screen.</li>
            <li>Each level represents an architecture challenge. You have <b>90 seconds</b> to resolve as many challenges as possible.</li>
            <li>Use your AWS knowledge to fill the missing squares, with the "correct" side of a block face up.</li>
            <li>Once you have filled in all the missing squares, <b>push the button</b> (don't forget) to submit your answer.</li>
            </ul>
            <h3>Colour Code</h3>
            <ul class="colour-code">
            <li>Compute & Network</li>
            <li>IoT, Big Data & Machine Learning</li>
            <li>Databases</li>
            <li>Application & Mobile Services</li>
            <li>Storage & Content Delivery</li>
            <li>Dev & Management Tools</li>
            </ul>
            </div>
            </div>
            
            )
            
            );
          }
        }
        