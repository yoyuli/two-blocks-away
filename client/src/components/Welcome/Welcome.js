import React, { Component } from 'react';
import './Welcome.css';
import background from './cover.svg';
import { string, func } from 'prop-types';
import Timer from '../Timer/Timer';

class Welcome extends Component {
  static propTypes = {
    name: string,
    finishedTutorial: func
  };

  doesPlayerExist() {
    return this.props.name;
  }

  render() {
    return (
      <div className="Welcome">
        <img className="stretch back" src={background} alt="home" />
        <div className="hello front">
          {this.doesPlayerExist() ? `Hello, ${this.props.name}` : 'Waiting for the next player!' }
        </div>
        <div className="instructions front">
          {this.doesPlayerExist() ? <Timer finished={this.props.finishedTutorial} running maxTime={10} /> : '' }
        </div>
      </div>
    );
  }
}

export default Welcome;
