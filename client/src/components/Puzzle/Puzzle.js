import React, { Component } from 'react';
import './Puzzle.css';
import twoTierWeb from './images/two-tier-web.svg';
import serverlessWeb from './images/serverless-web.svg';
import serverlessMobile from './images/serverless-mobile.svg';
import eventStream from './images/event-stream.svg';
import centralisedLog from './images/centralised-log.svg';
import twoBlocks from './images/two-blocks.svg';
import machineLearning from './images/machine-learning.svg';
import dataLake from './images/data-lake.svg';
import continuousDeployment from './images/continuous-deployment.svg';
import vdi from './images/vdi.svg';
import cloudBackup from './images/cloud-backup.svg';
import containerBased from './images/container-based.svg';
import inMemoryCache from './images/in-memory-cache.svg';
import vod from './images/vod.svg';
import iotPayload from './images/iot-payload.svg';
import continuousSecurity from './images/continuous-security.svg';
import oracleMigration from './images/oracle-migration.svg';
import cloudApplication from './images/cloud-application.svg';
import recommendationEngine from './images/recommendation-engine.svg';

import { string } from 'prop-types';

export default class Puzzle extends Component {
  static propTypes = {
    level: string
  }

  constructor(props) {
    super(props);

    this.images = {
      twoTierWeb:twoTierWeb,
      serverlessWeb:serverlessWeb,
      serverlessMobile:serverlessMobile,
      eventStream:eventStream,
      centralisedLog:centralisedLog,
      twoBlocks:twoBlocks,
      machineLearning:machineLearning,
      dataLake:dataLake,
      continuousDeployment:continuousDeployment,
      vdi:vdi,
      cloudBackup:cloudBackup,
      containerBased:containerBased,
      inMemoryCache:inMemoryCache,
      vod:vod,
      iotPayload:iotPayload,
      continuousSecurity:continuousSecurity,
      oracleMigration:oracleMigration,
      cloudApplication:cloudApplication,
      recommendationEngine:recommendationEngine
    };
  }

  render() {
    return (
      <div className="Puzzle">
        <img className="stretch" src={this.images[this.props.level]} alt="puzzle piece" />
      </div>
    );
  }
}
