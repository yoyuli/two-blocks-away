import React, { Component } from 'react';
import './Information.css';
import correct from './images/correct.svg';
import incorrect from './images/incorrect.svg';
import Timer from '../Timer/Timer';

class Information extends Component {
  render() {
    const image = this.props.solution ? correct : incorrect;
    return (
      <div className="Information">
        <img className="center" src={image} alt="solution answer" />
        <Timer finished={this.props.finished} running maxTime={2} shouldHide />
      </div>
    );
  }
}

export default Information;
