import React, { Component } from 'react';
import './CheckingSolution.css';
import Timer from '../Timer/Timer';

class CheckingSolution extends Component {
  render() {
    return (
      <div className="CheckingSolution">
        <div className="hello front">Checking Solution...</div>
        <Timer running shouldHide maxTime={15} finished={this.props.timeout} />
      </div>
    );
  }
}

export default CheckingSolution;
