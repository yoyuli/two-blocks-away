import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Information from '../Information/Information';
import Puzzle from '../Puzzle/Puzzle';
import Timer from '../Timer/Timer';
import Welcome from '../Welcome/Welcome';
import CheckingSolution from '../CheckingSolution/CheckingSolution';
import device from '../../libs/iot-device';
import shadows from '../../libs/iot-shadow';
import gameover from './images/gameover.svg';
import './Game.css';

class Game extends Component {
  constructor(props) {
    super(props);
    
    this.gameTime = process.env.GAME_TIME?process.env.GAME_TIME:90;
    
    this.LEVELS = {
      0:{
        0:'twoTierWeb'
      }, 
      1:{
        0:'serverlessWeb',
        1:'serverlessMobile',
        2:'twoBlocks',
        3:'dataLake'
      },
      2:{
        0:'eventStream',
        1:'centralisedLog',
        2:'vdi',
        3:'machineLearning',
        4:'cloudBackup',
        5:'containerBased',
        6:'inMemoryCache',
        7:'vod',
        8:'iotPayload',
        9:'oracleMigration'
      },
      3:{
        0:'continuousSecurity',
        1:'cloudApplication'
      },
      4:{
        0:'recommendationEngine'
      }
    };
    
    this.TOTAL_LEVELS=Object.keys(this.LEVELS).length; 
    
    this.state = {
      player: { name: '', id: null},
      currentLevel: [0,0], // prop for Puzzle
      currentLevelName:this.LEVELS['0']['0'],
      levelMarker:{},
      currentTime: 0, // prop for Timer
      score: 0,
      levelsCompleted:0,
      waitingForNewPlayer: true,
      checkingSolution: false, // prop for Timer (if checkingSolution, then Timer is paused)
      showSolution: false,
      latestAttempt: false,
      gameOver: false,
      lastCheckedButtonPushTime:0,
      lastResolvedButtonPushTime:0
    };
    
    /*
    levelMarker
    e.g. If level 0-0, 1-0 and 1-1 have been resolved,
    it is represented in the levelMarker as:
    levelMarker={
      0:[0],
      1:[0,1]
    }
    */
  }
  
  startNewGame() {
    this.setState({
      waitingForNewPlayer: false,
      score: 0,
      levelsCompleted:0,
      currentLevel:[0,0],
      currentLevelName:this.LEVELS['0']['0'],
      levelMarker:{
        0:[],
        1:[],
        2:[],
        3:[],
        4:[]
      },
      currentTime: this.gameTime,
      showSolution: false,
      latestAttempt: false
    });
    // update game shadow after 20ms
    setTimeout(this.updateGameShadow.bind(this),20);
  }
  
  hideSolution() {
    this.setState({ showSolution: false });
  }
  
  hideGameOver() {
    this.setState({ gameOver: false });
  }
  
  handleNewPlayer(name, id) {
    this.setState({
      player: {
        name: name,
        id: id
      }
    });
  }
  
  handleGameOver() {
    const finalState = {
      name: this.state.player.name, 
      id: this.state.player.id, 
      level: this.state.currentLevelName, 
      levelMarker: this.state.levelMarker, 
      time: this.state.currentTime
    };
    device.publish(process.env.REACT_APP_AWS_IOT_TOPIC_GAME_FINISH, JSON.stringify(finalState));
    
    this.setState({
      player: { name: '', id: undefined },
      gameOver: true,
      waitingForNewPlayer: true
    });
  }
  
  randomInt(min,max) { // min included and max not included
    return Math.floor(Math.random()*(max-min)+min);
  }  
  
  markLevelAsDone(currentLevel) {
    if (!this.state.levelMarker[currentLevel[0]]) {
      let levelMarker = this.state.levelMarker;
      levelMarker[currentLevel[0]] = [];
      this.setState({ levelMarker: levelMarker });
    }
    if (!this.state.levelMarker[currentLevel[0]].includes(currentLevel[1])) {
      let levelMarker = this.state.levelMarker;
      levelMarker[currentLevel[0]].push(currentLevel[1]);
      this.setState({ levelMarker: levelMarker });
    } else {
      console.log('WARNING: level already marked as complete. ');
    }
    return true;
  }
  
  findNextLevel(currentLevel) {
    console.log('Current level: '+currentLevel[0]+'-'+currentLevel[1]);
    let nextLevel=[];
    let totalSublevelsCompleted=this.state.levelMarker[currentLevel[0]].length;
    let totalSublevelsCurrent=Object.keys(this.LEVELS[currentLevel[0]]).length;
    nextLevel[0]=currentLevel[0];
    let totalSublevelsNext=totalSublevelsCurrent;
    if(totalSublevelsCompleted>=totalSublevelsCurrent){
      nextLevel[0]=currentLevel[0]+1;
      if(this.LEVELS[nextLevel[0]]){   // The next level exists
        totalSublevelsNext=Object.keys(this.LEVELS[nextLevel[0]]).length;
      }else{
        console.log('No more levels left')
        return false; 
      }
    }
    do{
      console.log('Looking for a random sublevel')
      nextLevel[1]=this.randomInt(0,totalSublevelsNext); 
    }
    while(this.state.levelMarker[nextLevel[0]]        // Only when next level exists in the marker
      && this.state.levelMarker[nextLevel[0]].includes(nextLevel[1]) //  And the sublevel already marked
      )
      console.log('Next level: '+nextLevel[0]+'-'+nextLevel[1]);
      return nextLevel; 
    }
    
    handleSolution(solution) {
      if (this.state.waitingForNewPlayer) {
        return;
      }
      
      if(solution.buttonPushed && solution.buttonPushed>this.state.lastResolvedButtonPushTime){
        this.state.lastResolvedButtonPushTime=solution.buttonPushed;
        this.state.score=solution.score? solution.score: this.state.score;
        this.state.levelsCompleted=solution.levelsCompleted? solution.levelsCompleted: this.state.levelsCompleted;
        
        
        if (solution.isCorrect) {
          this.markLevelAsDone(this.state.currentLevel);
          let nextLevel = this.findNextLevel(this.state.currentLevel);
          if (!nextLevel) { // make sure that the next level exists
            this.handleGameOver();
          } else {
            this.setState({
              currentLevel:nextLevel,
              currentLevelName: this.LEVELS[nextLevel[0]][nextLevel[1]],
            });
          }
        }
        
        this.setState({
          showSolution: true, 
          latestAttempt: solution.isCorrect,
          checkingSolution: false
        });
      }else{
        console.error("missing buttonPushed attribute in the message");
      }
     
    }
    
    // TODO - @yoyu
    handleSolutionTimeout() {
      // some logic would go in here that handles the timeout however you need
      
      // option 1: change the message on CheckingSolution component
      /*
      <CheckingSolution /> would need to take a `text` prop so that you can change the text:
      this.setState({
        checkingSolutionText: "Still checking, hold on!"
      });
      */
      
      // option 2: cancel the solution check
      this.setState({
        latestAttempt: false,
        checkingSolution: false
      });

      // update game shadow after 20ms
      setTimeout(this.updateGameShadow.bind(this),20);
    }
    
    handleCheck(payload) {
      if (this.state.waitingForNewPlayer) {
        return;
      }
      
      try {
        if(payload.buttonPushed>this.state.lastCheckedButtonPushTime){
          this.state.lastCheckedButtonPushTime=payload.buttonPushed;
          this.setState({ checkingSolution: true });
        }
      } catch (e) {
        console.error('Failed to handle check');
      }
    }
    
    timerChange(newTime) {
      this.setState({
        currentTime: newTime
      });
      
      if (newTime <= 0) {
        this.handleGameOver();
      }
    }
    
    updateGameShadow() {
      const gameState = { 
        state: { 
          desired: { 
            playerId: this.state.player.id, 
            gameStatus: this.getGameStatus(),
            name: this.state.player.name,
            level: this.state.currentLevelName, 
            time: this.state.currentTime,
            levelMarker: this.state.levelMarker, 
          } 
        } 
      };
      //console.log(JSON.stringify(gameState.state));
      const clientTokenUpdate = shadows.update(process.env.REACT_APP_AWS_IOT_SHADOW_GAME, gameState);
      if (clientTokenUpdate === null) {
        console.error('Error updating shadow');
      } else {
        console.log('Game shadow updated');
      }
    }
    
    getGameStatus() {
      // console.log(JSON.stringify(this.state));
      if (this.state.waitingForNewPlayer) { 
        return 'idle';
      } else if (this.state.checkingSolution) {
        return 'busy'
      }
      return 'waiting';
    }
    
    renderGame() {
      if (this.state.checkingSolution) {
        return <div><CheckingSolution timeout={this.handleSolutionTimeout.bind(this)} /></div>;
      } else if (this.state.showSolution) {
        return <div><Information solution={this.state.latestAttempt} finished={this.hideSolution.bind(this)} /></div>
      } else {
        return (
          <div>
          <Puzzle level={this.state.currentLevelName} />
          <div className="gameTimer">
          <Timer running={!this.state.checkingSolution && this.state.currentTime > 0} maxTime={this.state.currentTime} tick={this.timerChange.bind(this)} />
          </div>
          <div className="playerScore">
          <ul>
          <li>Level: {this.state.levelsCompleted+1}</li>
          <li>Score: {this.state.score}</li>
          </ul>
          </div>
          </div>
          );
        }
      }
      
      renderWelcome() {
        return <Welcome name={this.state.player.name} finishedTutorial={this.startNewGame.bind(this)} />;
      }
      
      renderGameOver() {
        return (
          <div className="gameOver">
          <img src={gameover} alt="game over" />
          <Timer running maxTime={5} finished={this.hideGameOver.bind(this)} />
          <div className="finalScore">
          <ul>
          <li>Levels Completed: {this.state.levelsCompleted}</li>
          <li>Score: {this.state.score}</li>
          </ul>
          </div>
          </div>
          );
        }
        
        componentDidMount() {
          device.on('message', (topic, payload) => {
            console.log('received message');
            console.log(`topic = ${topic}`);
            console.log(payload.toString());
            if (topic === process.env.REACT_APP_AWS_IOT_TOPIC_GAME_START) {
              const { playerName, playerId } = JSON.parse(payload.toString());
              this.handleNewPlayer(playerName, playerId);
            } else if (topic === process.env.REACT_APP_AWS_IOT_TOPIC_GAME_SOLUTION) {
              this.handleSolution(JSON.parse(payload.toString()));
            } else if (topic === process.env.REACT_APP_AWS_IOT_TOPIC_GAME_CHECK) {
              this.handleCheck(JSON.parse(payload.toString()));
            }
            this.updateGameShadow();
          });
          
          shadows.on('connect', () => {
            shadows.register(process.env.REACT_APP_AWS_IOT_SHADOW_GAME, () => {
              this.updateGameShadow();
            });
          });
        }
        
        render() {
          return (
            <div className="Game">
            { 
              this.state.gameOver ? this.renderGameOver() :
              this.state.waitingForNewPlayer ? this.renderWelcome() : this.renderGame() 
            }
            </div>
            );
          }
        }
        
        export default withRouter(Game);
        