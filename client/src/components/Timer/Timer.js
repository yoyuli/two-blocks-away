import React, { Component } from 'react';
import './Timer.css';
import { bool, number, func } from 'prop-types';

export default class Timer extends Component {

  static propTypes = {
    running: bool,
    maxTime: number,
    finished: func
  };

  constructor(props) {
    super(props);
    this.timer = undefined;
    this.interval = 1000;

    this.state = {
      time: this.props.maxTime
    };
  }

  setTime(seconds) {
    if (this.timer) {
      this.setState({time: seconds});
    }
  }

  decrementTimer() {
    this.setTime(this.state.time - 1);
    if (this.state.time < 0) { // reset timer once it reaches 0
      if (typeof this.props.finished === 'function') {
        this.props.finished();
      }
      this.reset();
    }
    if (typeof this.props.tick === 'function') {
      this.props.tick(this.state.time);
    }
  }

  start() {
    const _this = this;
    if (!this.timer) { // make sure a timer isn't already running
      this.timer = setInterval(() => { _this.decrementTimer(_this.interval); }, _this.interval);
    }
  }

  stop() {
    this.killTimer();
  }

  reset() {
    this.killTimer();
    this.setTime(this.props.maxTime);
  }

  killTimer() {
    clearInterval(this.timer);
    this.timer = undefined;
  }

  componentWillUnmount() {
    this.killTimer();
  }

  render() {
    if (this.props.running) {
      this.start();
    } else {
      this.stop();
    }

    return (
      <div className={`Timer ${this.props.shouldHide ? 'hidden' : ''} `}>
        {this.state.time}
      </div>
    );
  }
}
