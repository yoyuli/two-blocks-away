const fs = require('fs');

const AWS = require('aws-sdk');

const region = process.env.AWS_DEFAULT_REGION
AWS.config.region = region;

const stage=process.env.TBA_STAGE;

async function run() {
  let envs = [];
  envs.push({ REACT_APP_AWS_REGION: region });
  envs.push({ REACT_APP_AWS_IOT_ENDPOINT: process.env.TBA_IOT_ENDPOINT }); 
  envs.push({ REACT_APP_AWS_IOT_SHADOW_GAME: 'TBA_Game_'+stage });
  envs.push({ REACT_APP_AWS_IOT_SHADOW_LEADERBOARD: 'TBA_Leaderboard_'+stage });
  envs.push({ REACT_APP_AWS_IOT_TOPIC_GAME_START: 'TBA/'+stage+'/Start'});
  envs.push({ REACT_APP_AWS_IOT_TOPIC_GAME_FINISH: 'TBA/'+stage+'/Finish' });
  envs.push({ REACT_APP_AWS_IOT_TOPIC_GAME_SOLUTION: 'TBA/'+stage+'/Solution'});
  envs.push({ REACT_APP_AWS_IOT_TOPIC_GAME_CHECK: 'TBA/'+stage+'/ButtonPush' });
  envs.push({ REACT_APP_AWS_IOT_TOPIC_LEADERBOARD_CHANGE: 'TBA/'+stage+'/LeaderboardChange' });
  envs.push({ REACT_APP_AWS_LAMBDA_LEADERBOARD: 'tba-lambda-score-'+stage+'-scoresChange'});
  envs.push({ REACT_APP_AWS_DYNAMODB_USERS: process.env.SCEPTRE_UserTableName});
  envs.push({ REACT_APP_AWS_IDENTITY_POOL_ID: process.env.SCEPTRE_IdentityPoolId });
  
  const fileContents = envs.map(env => {
    const key = Object.keys(env)[0];
    return `${key}=${env[key]}`;
  }).join('\n');

  fs.writeFile('./.env', fileContents, (err) => {
    if (err) { return console.log(err); }
    console.log("The file was saved!");
  });
}

run();
