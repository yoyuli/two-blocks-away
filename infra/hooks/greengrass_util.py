import os

import logging

LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO').upper()
logging.basicConfig(
    level=LOGLEVEL,
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
)

logger = logging.getLogger('greengrass_util')

import boto3

gg_client = boto3.client('greengrass')
iot_client=boto3.client('iot')


def write_text_to_file(text,file_name):
    text_file = open(file_name, "w")
    text_file.write(text)
    text_file.close()
    logger.info('Wrote to file '+file_name )
    return 

def create_certs(thing_name):
    if not os.path.exists('certs'):
        os.makedirs('certs')
    
    response=iot_client.create_keys_and_certificate(
                setAsActive=True
            )
    
    write_text_to_file(response['certificatePem'],'certs/%s-cert.pem' % thing_name)
    write_text_to_file(response['keyPair']['PublicKey'],'certs/%s-key.public'% thing_name)
    write_text_to_file(response['keyPair']['PrivateKey'],'certs/%s-key.private'% thing_name)
    
    return response['certificateArn']

