from setuptools import setup

setup(
    name='custom_hooks',
    py_modules=['custom_hooks','greengrass_util'],
    entry_points={
        'sceptre.hooks': [
            'greengrass_certs = custom_hooks:greengrass_certs'
        ],
    }
)