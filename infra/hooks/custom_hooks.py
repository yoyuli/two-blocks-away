from sceptre.hooks import Hook
import os
import yaml

import logging
LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO').upper()
logging.basicConfig(
    level=LOGLEVEL,
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
)

logger = logging.getLogger('sceptre_custom_hooks')

from greengrass_util import write_text_to_file
from greengrass_util import create_certs

class greengrass_certs(Hook):
    def __init__(self, *args, **kwargs):
        super(greengrass_certs, self).__init__(*args, **kwargs)

    def run(self):
        logger.info('Executing greengrass_certs tasks')

        stage= os.environ['TBA_STAGE']

        config=self.stack.stack_group_config

        camera_cert_arn=create_certs(config['camera_name'])
        write_text_to_file(camera_cert_arn,'vars/camera_cert_arn_'+stage+'.txt')
        
        button_cert_arn=create_certs(config['button_name'])
        write_text_to_file(button_cert_arn,'vars/button_cert_arn_'+stage+'.txt')
