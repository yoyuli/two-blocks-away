
export SCEPTRE_IdentityPoolId=eu-west-1:38e9adcf-de78-47f7-8663-47700c545957
export SCEPTRE_ScoreTableName=TBA_Score_Table_dev
export SCEPTRE_TableARN=arn:aws:dynamodb:eu-west-1:076896347854:table/TBA_Score_Table_dev
export SCEPTRE_UserTableName=TBA_User_Table_dev
export SCEPTRE_StreamARN=arn:aws:dynamodb:eu-west-1:076896347854:table/TBA_Score_Table_dev/stream/2019-05-02T18:51:56.814
export SCEPTRE_TBACloudFrontOriginAccessIdentity=E2T4TGW2QWZC9
export SCEPTRE_WebsiteURL=http://tba-infra-dev-s3-tbawebbucket-1lt65drjggz4n.s3-website-eu-west-1.amazonaws.com
export SCEPTRE_TBAWebBucketName=tba-infra-dev-s3-tbawebbucket-1lt65drjggz4n
export SCEPTRE_TBAImageBucketName=tba-infra-dev-s3-tbaimagebucket-s3e5tcjjg6hv
