Description: Create CoreDefinition with InitialVersion
Parameters:
  GroupName:
    Type: String
  TBAGameName:
    Type: String
  CameraName:
    Type: String
  CameraARN:
    Type: String
  CameraCertARN:
    Type: String
  ButtonName:
    Type: String
  ButtonARN:
    Type: String
  ButtonCertARN:
    Type: String
  CaptureFunctionArn:
    Type: String
  ValidateFunctionArn:
    Type: String
  ButtonPushedTopic:
    Type: String
  CaptureTopic:
    Type: String
  TBAImageBucketName:
    Type: String
  TBACaptureTopicRuleName:
    Type: String
  
Resources:
  GreengrassGroup:
    Type: AWS::Greengrass::Group
    Properties : 
      Name : !Ref GroupName
      RoleArn : !GetAtt GreengrassGroupRole.Arn
  GreengrassGroupRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "greengrass.amazonaws.com" 
            Action: 
              - "sts:AssumeRole"
      Path: "/"
      Policies:
        -
          PolicyName: "GreengrassGroupRolePolicy"
          PolicyDocument:
            Version: "2012-10-17"
            Statement:
              -   
                  Effect: "Allow"
                  Action: "iot:*"
                  Resource: "*"
              -  
                  Effect: "Allow"
                  Action: "s3:*"
                  Resource: "*"
  CameraDefinition:
    Type: 'AWS::Greengrass::CoreDefinition'
    Properties:
      Name: !Ref CameraName
  CameraDefinitionVersion:
    Type: 'AWS::Greengrass::CoreDefinitionVersion'
    Properties:
      CoreDefinitionId: !Ref CameraDefinition
      Cores:
        - Id: !Ref CameraName
          CertificateArn: !Ref CameraCertARN
          ThingArn: !Ref CameraARN
          SyncShadow: 'true'
  ButtonDefinition:
    Type: 'AWS::Greengrass::DeviceDefinition'
    Properties:
      Name: !Ref ButtonName
  ButtonDefinitionVersion:
    Type: 'AWS::Greengrass::DeviceDefinitionVersion'
    Properties:
      DeviceDefinitionId: !Ref ButtonDefinition
      Devices:
        - Id: !Ref ButtonName
          ThingArn: !Ref ButtonARN
          CertificateArn: !Ref ButtonCertARN
          SyncShadow: 'true'
  CaptureFunctionDefinition:
    Type: AWS::Greengrass::FunctionDefinition
    Properties : 
      Name : CaptureFunction
  CameraResourceDefinition:
    Type: 'AWS::Greengrass::ResourceDefinition'
    Properties:
      Name: CameraResourceDefinition
  CameraResourceDefinitionVersion:
    Type: 'AWS::Greengrass::ResourceDefinitionVersion'
    Properties:
      ResourceDefinitionId: !Ref CameraResourceDefinition
      Resources:
        - Id: VideoCoreSharedMemory
          Name: VideoCoreSharedMemory
          ResourceDataContainer:
            LocalDeviceResourceData:
              SourcePath: /dev/vcsm
              GroupOwnerSetting:
                AutoAddGroupOwner: 'true'
        - Id: VideoCoreInterface
          Name: VideoCoreInterface
          ResourceDataContainer:
            LocalDeviceResourceData:
              SourcePath: /dev/vchiq
              GroupOwnerSetting:
                AutoAddGroupOwner: 'true'
        - Id: LocalVolume
          Name: LocalVolume
          ResourceDataContainer:
            LocalVolumeResourceData:
              SourcePath: /tmp/
              DestinationPath: /local/
              GroupOwnerSetting:
                AutoAddGroupOwner: 'true'
  CaptureFunctionDefinitionVersion:
    Type: 'AWS::Greengrass::FunctionDefinitionVersion'
    Properties:
      FunctionDefinitionId: !GetAtt 
        - CaptureFunctionDefinition
        - Id
      DefaultConfig:
        Execution:
          IsolationMode: GreengrassContainer
      Functions:
        - Id: CaptureFunction
          FunctionArn: !Ref CaptureFunctionArn
          FunctionConfiguration:
            MemorySize: '262144'
            Timeout: '15'
            Environment:
              Variables:
                CALLBACK_TOPIC: !Ref CaptureTopic
                IMAGE_BUCKET_NAME: !Ref TBAImageBucketName
                THING_NAME: !Ref TBAGameName
                AWS_DEFAULT_REGION: !Ref AWS::Region
              ResourceAccessPolicies:
                - ResourceId: VideoCoreSharedMemory
                  Permission: rw
                - ResourceId: VideoCoreInterface
                  Permission: rw
                - ResourceId: LocalVolume
                  Permission: rw
              AccessSysfs: 'false'
  SubscriptionDefinition:
    Type: 'AWS::Greengrass::SubscriptionDefinition'
    Properties:
      Name: SubscriptionDefinition
  SubscriptionDefinitionVersion:
    Type: 'AWS::Greengrass::SubscriptionDefinitionVersion'
    Properties:
      SubscriptionDefinitionId: !Ref SubscriptionDefinition
      Subscriptions:
        - Id: ButtonPushedCloudToGreengrass
          Source: cloud
          Subject: !Ref ButtonPushedTopic
          Target: !Ref CaptureFunctionArn
        - Id: CaptureGreengrassToCloud
          Source: !Ref CaptureFunctionArn
          Subject: !Ref CaptureTopic
          Target: cloud
  CaptureTopicRule:
    Type: AWS::IoT::TopicRule
    Properties : 
      RuleName: !Ref TBACaptureTopicRuleName
      TopicRulePayload : 
        RuleDisabled : false
        Sql : !Sub 'SELECT * FROM "${CaptureTopic}"'
        Actions : 
          - 
            Lambda:
               FunctionArn: !Ref ValidateFunctionArn
  LambdaInvokePermission:
    Type: AWS::Lambda::Permission
    Properties:
      FunctionName: !Ref ValidateFunctionArn
      Action: 'lambda:InvokeFunction'
      Principal: iot.amazonaws.com
      SourceArn: !Sub 'arn:aws:iot:${AWS::Region}:${AWS::AccountId}:rule/${TBACaptureTopicRuleName}'
  GreengrassGroupVersion:
    Type: AWS::Greengrass::GroupVersion
    Properties : 
      GroupId : !GetAtt GreengrassGroup.Id
      CoreDefinitionVersionArn : !Ref CameraDefinitionVersion
      DeviceDefinitionVersionArn : !Ref ButtonDefinitionVersion
      FunctionDefinitionVersionArn : !Ref CaptureFunctionDefinitionVersion
      ResourceDefinitionVersionArn : !Ref CameraResourceDefinitionVersion
      SubscriptionDefinitionVersionArn : !Ref SubscriptionDefinitionVersion
