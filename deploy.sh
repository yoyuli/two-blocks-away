. config.sh

virtualenv --python=python3.7 venv
source venv/bin/activate
pip install -r requirements.txt

TBA_IAC_ROOT=$(pwd)

cd $TBA_IAC_ROOT/infra
echo "" > outputs.sh
cd hooks/; python setup.py install; cd ..

echo "Creating Cognito Stack"
sceptre launch cognito --yes
sceptre list outputs cognito -e envvar >> outputs.sh

echo "Creating DynamoDB Stack"

sceptre launch dynamodb --yes
sceptre list outputs dynamodb -e envvar >> outputs.sh

echo "Creating S3 Stack"

sceptre launch s3 --yes
sceptre list outputs s3 -e envvar >> outputs.sh

echo "Creating IoT Thins"

sceptre launch iot-things --yes
sceptre list outputs iot-things  -e envvar >> outputs.sh

sceptre launch thing-certs --yes
sceptre list outputs thing-certs  -e envvar >> outputs.sh

. outputs.sh

cd $TBA_IAC_ROOT

echo "Deploying Greengrass Lambda functions"

cd $TBA_IAC_ROOT/lambdas/greengrass
# Greengrass Lambda functions will reference the S3 bucket created in the previous step
sls deploy
cd $TBA_IAC_ROOT


echo "Deploying validateSolution Lambda functions"

cd $TBA_IAC_ROOT/lambdas/validateSolution
sls deploy
cd $TBA_IAC_ROOT

echo "Updating Greengrass Stack"

cd $TBA_IAC_ROOT/infra
# This update will reference the greengrass and validate lambda functions
# Therefore, needs to be performed aft.der lambdas/greengrass is deployed
sceptre launch greengrass --yes
cd $TBA_IAC_ROOT

echo "Deploying Scores Lambda functions"

cd $TBA_IAC_ROOT/lambdas/scores
sls deploy
cd $TBA_IAC_ROOT




cd $TBA_IAC_ROOT/client
npm run deploy
aws s3 sync build/ s3://$SCEPTRE_TBAWebBucketName
cd $TBA_IAC_ROOT

deactivate