const AWS = require('aws-sdk');
const awsIot = require('aws-iot-device-sdk');
const EventEmitter = require('events');
const localEvents=new EventEmitter();
const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

const config={
    thingName: 'TBA_Core',
    iotEndPoint: 'a1x116mxnd5xyt.iot.us-east-1.amazonaws.com',
    buttonPushTopic:'Hackathon/Game/buttonPush',
    cameraCaptureTopic:'Hackathon/Game/Capture',
    solutionTopic:'Hackathon/Game/Solution'
}

const testScenarios={
    1:{
        0:'900010000',
        1:'900010001'
    }
}

const iotdata = new AWS.IotData({ 
    endpoint: config.iotEndPoint,
    region:'us-east-1',
    apiVersion: '2015-05-28'
});

global.client = awsIot.device({
    host: config.iotEndPoint,
    clientId:'test-client-'+Date.now(),
    protocol:'wss'
});

const connectIotTopic=()=>new Promise((resolve,reject)=>{
    global.client.on('connect', function() {
        console.log('connect');
        client.subscribe(config.solutionTopic);
        resolve();
    });
});


global.client.on('message', function(topic, payload) {
    // console.log('message', topic, payload.toString());
    localEvents.emit(topic,payload);
});

const pushButton=()=>{
    console.log('Pushing the button');
    let params = {
        topic: config.buttonPushTopic,
        payload: JSON.stringify({
            buttonPushed: Date.now()
        }),
        qos: 0
    };
    return iotdata.publish(params).promise();
}

const captureImage=(level,shouldBeCorrect)=>{
    console.log('Taking a fake picture');
    let params = {
        topic: config.cameraCaptureTopic,
        payload: JSON.stringify({
            buttonPushed: testScenarios[level][shouldBeCorrect],
            backdoorMode:true   // disable button timestamp check
        }),
        qos: 0
    };
    return iotdata.publish(params).promise();
}

const testLevel= (level,shouldBeCorrect)=> new Promise(async(resolve,reject)=>{
    let timer=setTimeout(()=>{
        reject('timeout');
    },10000);
    console.log('\nTesting Level '+level);
    localEvents.on(config.solutionTopic,(payload)=>{
        let isCorrect=JSON.parse(payload.toString()).isCorrect;
        console.log(isCorrect?'Correct':'Not correct');
        clearTimeout(timer);
        resolve(isCorrect);
    });
    await pushButton();
    console.log('This should be '+(shouldBeCorrect?'correct':'incorrect'));
    await captureImage(level,shouldBeCorrect);
});

const runTests=async ()=>{
    await connectIotTopic();
    try{
        await testLevel(1,0);
        console.log('\nLet me think...')
        await delay(1500);
        await testLevel(1,1);
    }catch(e){
        console.error(e);
    }
    
}

runTests();