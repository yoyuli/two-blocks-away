const randomInt=(min,max) =>    // min included and max not included
    Math.floor(Math.random()*(max-min)+min);

const LEVELS={
    0:{
        0:'two-tier-web'
    },
    1:{
        0:'serverless-web',
        1:'serverless-mobile'
    },
    2:{
        0:'event-stream',
        1:'deployment-pipeline',
        2:'centralised-log',
        3:'two-blocks'
    },
    3:{
        0:'continuous-security',
        1:'oracle-data'
    },
    4:{
        0:'hybrid-cloud'
    }
}

const TOTAL_LEVELS=Object.keys(LEVELS).length; 

let levelMarker={}
    /*
        e.g. If level 0-0, 1-0 and 1-1 have been resolved,
        it is represented in the levelMarker as:
        levelMarker={
            0:[0],
            1:[0,1]
        }
    */

const findNextLevel=(currentLevel)=>{
    
    const markLevelAsDone=(currentLevel)=>{
        if(!levelMarker[currentLevel[0]]){
            levelMarker[currentLevel[0]]=[];
        }
        if(!levelMarker[currentLevel[0]].includes(currentLevel[1])){
            levelMarker[currentLevel[0]].push(currentLevel[1]);
        }else{
            console.log('WARNING: level already marked as complete. ');
        }
        return true;
    }
    markLevelAsDone(currentLevel);
    console.log('Current level: '+currentLevel[0]+'-'+currentLevel[1]);
    let nextLevel=[];
    let totalSublevelsCompleted=levelMarker[currentLevel[0]].length;
    let totalSublevelsCurrent=Object.keys(LEVELS[currentLevel[0]]).length;
    nextLevel[0]=currentLevel[0];
    let totalSublevelsNext=totalSublevelsCurrent;
    if(totalSublevelsCompleted>=totalSublevelsCurrent){
        nextLevel[0]=currentLevel[0]+1;
        if(LEVELS[nextLevel[0]]){   // The next level exists
            totalSublevelsNext=Object.keys(LEVELS[nextLevel[0]]).length;
        }else{
            console.log('No more levels left')
            return false; 
        }
    }
    do{
        console.log('Looking for a random sublevel')
        nextLevel[1]=randomInt(0,totalSublevelsNext); 
    }while(levelMarker[nextLevel[0]]        // Only when next level exists in the marker
        && levelMarker[nextLevel[0]].includes(nextLevel[1]) //  And the sublevel already marked
    )
    console.log('Next level: '+nextLevel[0]+'-'+nextLevel[1]);
    return nextLevel; 
}

const calculateLevelsCompleted=()=>{
    let levelsCompleted=
            Object.keys(levelMarker)
            .map((key)=> levelMarker[key].length)
            .reduce((acc,length)=>(acc+length),0);
    return levelsCompleted;
}

let currentLevel=[0,0];

while(currentLevel){
    console.log(LEVELS[currentLevel[0]][currentLevel[1]]);
    currentLevel=findNextLevel(currentLevel);
    console.log('Completed '+calculateLevelsCompleted()+' levels');
}


