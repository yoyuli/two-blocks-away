export AWS_DEFAULT_REGION=eu-west-1;    # The AWS region this solution is deployed to
export TBA_ACCOUNT=076896347854;    # The AWS account number this solution is deployed to
export TBA_STAGE=dev;   


# Prerequisites:
# - AWS CLI
# - jq
export TBA_IOT_ENDPOINT=$(aws iot describe-endpoint --endpoint-type iot:Data-ATS| jq .endpointAddress -r)


# REACT_APP_AWS_REGION=us-east-1
# REACT_APP_AWS_IOT_ENDPOINT=a1x116mxnd5xyt-ats.iot.us-east-1.amazonaws.com

# REACT_APP_AWS_IOT_SHADOW_GAME=TBA_Core
# REACT_APP_AWS_IOT_SHADOW_LEADERBOARD=leaderboard
# REACT_APP_AWS_IOT_TOPIC_GAME_START=Hackathon/Game/Start
# REACT_APP_AWS_IOT_TOPIC_GAME_FINISH=Hackathon/Game/Finish
# REACT_APP_AWS_IOT_TOPIC_GAME_SOLUTION=Hackathon/Game/Solution
# REACT_APP_AWS_IOT_TOPIC_GAME_CHECK=Hackathon/Game/ButtonPush
# REACT_APP_AWS_IOT_TOPIC_LEADERBOARD_CHANGE=Hackathon/Leaderboard/Change
# REACT_APP_AWS_LAMBDA_LEADERBOARD=scoresChange
# REACT_APP_AWS_DYNAMODB_USERS=users
